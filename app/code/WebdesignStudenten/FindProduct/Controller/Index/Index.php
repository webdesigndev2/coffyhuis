<?php

namespace WebdesignStudenten\FindProduct\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action
{
    protected $_categoryFactory;
    protected $_resultFactory;
    
	public function __construct(
        \Magento\Catalog\Model\CategoryFactory $categoryFactory,
        \Magento\Framework\Controller\ResultFactory $resultFactory,
		\Magento\Framework\App\Action\Context $context
    ) {
        $this->_categoryFactory = $categoryFactory;  
        $this->_resultFactory = $resultFactory;  
		return parent::__construct($context);
	}
    
    /**
     * Get category object
     * Using $_categoryFactory
     *
     * @return \Magento\Catalog\Model\Category
     */
    public function getCategory($categoryId) 
    {
        $this->_category = $this->_categoryFactory->create();
        $this->_category->load($categoryId);        
        return $this->_category;
    }

	public function execute()
	{
        $slectedCategory = $this->getRequest()->getPost('catId');
        $childrens = $this->getCategory($slectedCategory)->getChildren();
        $print = '';
        if (!empty($childrens)) {
            $allChild = explode(',', $childrens);
            $print .= '<select>';
            foreach ($allChild as $child) {
                $childCat = $this->getCategory($child);
                $print .= '<option value="' . $child . '" catUrl="' . $childCat->getUrl() . '" style="color:#703A22;">'. $childCat->getName() .'</option>';
            }
            $print .= '</select>';
        }
        
        $resultRaw = $this->_resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_RAW);
        $resultRaw->setContents($print);
        return $resultRaw;
        
//        $responseSend = json_encode(array("paintHtml" => $print, "catUrl"=> $catURL));
//        $resultJson = $this->resultFactory->create(\Magento\Framework\Controller\ResultFactory::TYPE_JSON);
//        $resultJson->setData($responseSend);
//        return $resultJson;
	}
}