<?php
/**
 * @category   Mageants CookieLaw
 * @package    Mageants_CookieLaw
 * @copyright  Copyright (c) 2017 Mageants
 * @author     Mageants Team <support@Mageants.com>
 */
\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'Mageants_CookieLaw',
    __DIR__
);
