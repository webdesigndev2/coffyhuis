<?php
/**
  * @category   Mageants CookieLaw
  * @package    Mageants_CookieLaw
  * @copyright  Copyright (c) 2017 Mageants
  * @author     Mageants Team <support@Mageants.com>
  */
namespace Mageants\CookieLaw\Setup;
 
use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;
use Magento\Store\Model\StoreManagerInterface;

/**
 * @codeCoverageIgnore
 */
class InstallData implements InstallDataInterface
{
    protected $StoreManager;     
    /*** Init ** @param EavSetupFactory $eavSetupFactory */    
    public function __construct(StoreManagerInterface $StoreManager){        
        $this->StoreManager=$StoreManager;    
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $service_url = 'https://www.mageants.com/index.php/rock/register/live';
        $curl = curl_init($service_url);
        $curl_post_data = array('ext_name' => 'CookieLaw','dom_name'=>$this->StoreManager->getStore()->getBaseUrl());
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $curl_post_data);
        $curl_response = curl_exec($curl);
        curl_close($curl);

     }
}