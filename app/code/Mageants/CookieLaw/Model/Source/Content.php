<?php
/**
 * @category   Mageants CookieLaw
 * @package    Mageants_CookieLaw
 * @copyright  Copyright (c) 2017 Mageants
 * @author     Mageants Team <support@Mageants.com>
 */
namespace Mageants\CookieLaw\Model\Source;

class Content implements \Magento\Framework\Option\ArrayInterface
{
 public function toOptionArray()
 {
	 $options = [
		 [
			 'label' => __('Default Text'),
			 'value' => 'default',
		 ],
		 [
			 'label' => __('Custom Text'),
			 'value' => 'custom',
		 ],
	 ];
	 return $options;
	 }
}