<?php
/**
 * @category   Mageants CookieLaw
 * @package    Mageants_CookieLaw
 * @copyright  Copyright (c) 2017 Mageants
 * @author     Mageants Team <support@Mageants.com>
 */
namespace Mageants\CookieLaw\Model\Source;

class Position implements \Magento\Framework\Option\ArrayInterface
{
 public function toOptionArray()
 {
	 $options = [
		 [
			 'label' => __('Top'),
			 'value' => 'top',
		 ],
		 [
			 'label' => __('Bottom'),
			 'value' => 'bottom',
		 ],
	 ];
	 return $options;
	 }
}