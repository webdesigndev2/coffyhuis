<?php
/**
 * @category   Mageants CookieLaw
 * @package    Mageants_CookieLaw
 * @copyright  Copyright (c) 2017 Mageants
 * @author     Mageants Team <support@Mageants.com>
 */
namespace Mageants\CookieLaw\Model\Source;

class Notice implements \Magento\Framework\Option\ArrayInterface
{
    public function toOptionArray()
    {
        $options = [
            [
                'label' => __('Never show again'),
                'value' => 365,
            ],
            [
                'label' => __('Hide for the rest of the day'),
                'value' => 1,
            ],
            [
                'label' => __('Hide for the rest of the session'),
                'value' => 0,
            ],
        ];
        return $options;
    }
}